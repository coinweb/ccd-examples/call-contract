import {
    TxContext,
    NewTx,
    getContractId,
    continueTx,
    passCwebFrom,
    contractIssuer,
    store,
    genericClaim,
    claimKey,
    toHex,
    getParent,
    isSmartcontractIssuer,
    addDefaultMethodHandler,
    addMethodHandler,
    SELF_REGISTER_HANDLER_NAME,
    getContextTx,
    getContextCall,
    executeHandler,
} from "@coinweb/contract-kit";
import { selfRegisterHandler } from "@coinweb/self-register";

function logic(contextTx: TxContext): NewTx[] {
    const issuer = getContractId(contextTx);
    const contextCall = getContextCall();
    const parent = getParent(contextCall);

    if (!isSmartcontractIssuer(parent)) {
        throw "This contract should be called by another smartcontract";
    }

    return [
        continueTx([
            passCwebFrom(contractIssuer(issuer), 200),
            store(
                genericClaim(claimKey("It's key from receiver", "I was called"), 12345, toHex(0)),
            ),
        ]),
    ];
}

export function cwebMain() {
    addDefaultMethodHandler(logic);
    addMethodHandler(SELF_REGISTER_HANDLER_NAME, selfRegisterHandler);
    const contextTx = getContextTx();
    executeHandler(contextTx);
}
