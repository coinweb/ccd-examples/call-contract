import {
    NewTx,
    TxContext,
    getContractId,
    getContractArguments,
    getContractArgument,
    getMethodArguments,
    continueTx,
    passCwebFrom,
    contractIssuer,
    call,
    dataUnverified,
    DEFAULT_HANDLER_NAME,
    addDefaultMethodHandler,
    addMethodHandler,
    SELF_REGISTER_HANDLER_NAME,
    getContextTx,
    executeHandler,
} from "@coinweb/contract-kit";
import { selfRegisterHandler } from "@coinweb/self-register";

function logic(contextTx: TxContext): NewTx[] {
    const issuer = getContractId(contextTx);
    const provided = 1000;
    for (let i = 1; i < getContractArguments(contextTx).length; i++) {
        console.log(JSON.stringify(getContractArgument(contextTx, i)));
    }
    const receiverContractId = getMethodArguments(contextTx)[1];
    console.log("Receiver contract: ", JSON.stringify(receiverContractId));
    return [
        continueTx([
            passCwebFrom(contractIssuer(issuer), provided),
            call(1, receiverContractId),
            dataUnverified([DEFAULT_HANDLER_NAME]),
        ]),
    ];
}

export function cwebMain() {
    addDefaultMethodHandler(logic);
    addMethodHandler(SELF_REGISTER_HANDLER_NAME, selfRegisterHandler);
    const contextTx = getContextTx();
    executeHandler(contextTx);
}
